# Various low-level utilities

import re

import options



# Output

def debug(msg):
    if options.veryverbose:
        print(msg)
def info(msg):
    if options.verbose:
        print(msg)
def warn(msg):
    print('Warning:', msg)
def abort(msg):
    print('Error:', msg)
    exit(1)



# Quote a string
def quote(string):
    return "'" + re.sub(r"'", r"\'", string) + "'"
