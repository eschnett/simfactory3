# MDB, the machine database

import configparser
import glob
import re

from utils import *



__all__ = ['mdb']



# The MDB schema, i.e. the allowed keys and their types. It also
# defines human-readable descriptions and default values.
mdb_schema_entries = ['type', 'description', 'default']
mdb_schema_types   = ['boolean', 'int', 'float', 'string']



class MDBSchema:
    def __init__(self):
        info('Initialising MDB Schema...')
        self._schema = {}
        self.read_schema('etc/mdb_schema.ini')
        self.dump_schema()
    
    def __getitem__(self, key):
        return self._schema[key]
    
    def __iter__(self):
        return iter(self._schema)
    
    def __len__(self):
        return len(self._schema)
    
    def __repr__(self):
        return 'MDBSchema(%s)' % repr(self._schema)
    
    def __str__(self):
        return 'MDBSchema'
    
    def read_schema(self, filename):
        if not filename: abort('Empty MDB schema filename')
        # Read INI file
        config = configparser.ConfigParser()
        config.read(filename)
        for key in config.sections():
            try:
                self._schema[key]
                abort('MDB schema key "%s" exists already' % key)
            except KeyError:
                pass
            value_schema = {}
            for defn in config[key]:
                value = config[key][defn]
                if defn not in mdb_schema_entries:
                    abort('Illegal MDB schema definition "%s"' % defn)
                if defn == 'type' and value not in mdb_schema_types:
                    abort('Illegal MDB schema type "%s" for key "%s"' %
                          (value, key))
                value_schema[defn] = value
            self._schema[key] = value_schema
    
    def dump_schema(self):
        print('MDB schema:')
        for key in self._schema:
            print('    %s:' % key)
            value_schema = self._schema[key]
            for defn in mdb_schema_entries:
                # Output schema entry only if it exists
                try:
                    value = value_schema[defn]
                except KeyError:
                    continue
                print('        %s: %s' % (defn, value))



class MDBEntry:
    def __init__(self, schema, machine, iterable):
        self._schema = schema
        if not machine: abort('Empty machine name')
        self._machine = machine
        self._entry = {}
        self.init_entry()
        self.read_entry(iterable)
    
    def __getitem__(self, key):
        self.check_key(key)
        return self._entry[key]
    
    def __iter__(self):
        return iter(self._entry)
    
    def __len__(self):
        return len(self._entry)
    
    def __repr__(self):
        return ('MDBEntry(machine:%s,entry:%s)' %
                (self._machine, repr(self._entry)))
    
    def __str__(self):
        return 'MDBEntry(%s)' % self._machine

    def check_key(self, key):
        if not key: abort('Empty MDB key')
        # Look up key
        try:
            value_schema = self._schema[key]
        except KeyError:
            abort('MDB entry for machine "%s" '
                  'does not have an entry for key "%s"', (self._machine, key))
    
    def init_entry(self):
        # Set default values
        for key in self._schema:
            try:
                value = self._schema[key]['default']
            except KeyError:
                continue
            value = self._convert_type(key, value)
            self._entry[key] = value
    
    def read_entry(self, iterable):
        for key in iterable:
            # Check whether the key is legal
            try:
                value_schema = self._schema[key]
            except KeyError:
                abort('Illegal MDB key "%s" found for machine "%s"' %
                      (key, self._machine))
            value = iterable[key]
            value = self._convert_type(key, value)
            if key == 'machine':
                if value != self._machine:
                    abort('Value "%s" for key "%s" in INI file "%s" '
                          'is different from the machine name "%s"'
                          % (value, key, filename, self._machine))
            self._entry[key] = value
            # TODO: would this be a good idea?
            # setattr(self, key, value)
    
    def modify(self, iterable):
        for key in iterable:
            # Check whether the key is legal
            try:
                value_schema = self._schema[key]
            except KeyError:
                abort('Illegal MDB key "%s" found for machine "%s"' %
                      (key, self._machine))
            value = iterable[key]
            value = self._convert_type(key, value)
            if key == 'machine':
                abort('Cannot override value for key "machine"')
            self._entry[key] = value
    
    def _convert_type(self, key, value):
        # Check the value type
        type = self._schema[key]['type']
        if type == 'boolean':
            try:
                value = iterable.getboolean(key)
            except ValueError:
                abort('Value "%s" for key "%s" in INI file "%s" '
                      'is not boolean' % (value, key, filename))
        elif type == 'int':
            try:
                value = int(value)
            except ValueError:
                abort('Value "%s" for key "%s" in INI file "%s" '
                      'is not an int' % (value, key, filename))
        elif type == 'float':
            try:
                value = float(value)
            except ValueError:
                abort('Value "%s" for key "%s" in INI file "%s" '
                      'is not a float' % (value, key, filename))
        elif type == 'string':
            if value is None:
                abort('Value "%s" for key "%s" in INI file "%s" '
                      'is not a string' % (value, key, filename))
        else:
            abort('internal error')
        return value
    
    def dump_entry(self):
        print('MDB entry for machine "%s":' % self._machine)
        for key in self._entry:
            print('    %s = %s' % (key, self._entry[key]))



class MDB:
    def __init__(self):
        info('Initialising MDB...')
        self._schema = MDBSchema()
        self._mdb = {}
        for filename in glob.glob('mdb/*.ini'):
            self.read_entries(filename)
        self.read_overrides('etc/sim.ini')
        self.dump_mdb()
    
    def __getitem__(self, machine):
        self.check_machine_name(machine)
        return self._mdb[machine]
    
    def __iter__(self):
        return iter(self._mdb)
    
    def __len__(self):
        return len(self._mdb)
    
    def __repr__(self):
        return 'MDB(%s)' % repr(self._mdb)
    
    def __str__(self):
        return 'MDB'
    
    def check_machine_name(self, machine):
        if not machine: abort('Empty machine name')
        try:
            entry = self._mdb[machine]
        except KeyError:
            abort('Unknown machine name "%s"' % machine)
    
    def read_entries(self, filename):
        if not filename: abort('Empty filename')
        # Read INI file
        config = configparser.ConfigParser()
        config.read(filename)
        # Loop over all machines
        for machine in config.sections():
            try:
                entry = self._mdb[machine]
                abort('Machine "%s" exists already' % machine)
            except KeyError:
                pass
            self._mdb[machine] = MDBEntry(self._schema,
                                          machine, config[machine])
    
    def read_overrides(self, filename):
        if not filename: abort('Empty filename')
        # Read INI file
        config = configparser.ConfigParser()
        config.read(filename)
        # Loop over all machines
        for machine in config.sections():
            if not machine: abort('Empty machine name')
            try:
                entry = self._mdb[machine]
            except KeyError:
                warn('Ignoring unknown machine "%s"' % machine)
                continue
            self._mdb[machine].modify(config[machine])
    
    def dump_mdb(self):
        for machine in self._mdb:
            self._mdb[machine].dump_entry()



mdb = MDB()
